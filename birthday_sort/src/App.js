import {useState, useEffect} from "react"
import './App.css';
import ShowData from './component/ShowData';


function App() {
  const [Apidata, setApidata] = useState([]);
  const [sorted, setSorted] = useState([]);
  const url = "https://bitbucket.org/yashagrawal300/anuglar_learning/raw/23739869fef8fd9596ef1d15b4374df90a619ebd/birthday_sort/src/assets/data.json";
  const [dataType, setDataType] = useState("Received Data");
  const [sortedType, setSortedType] = useState("Sorted By Name");
  const [sortedColorChange, setsortedColorChange] = useState("THsortedByName");


  useEffect(() => {
    fetch(url, {
      method:"get"
    }).then(response => response.json()
    ).then(data =>{
        setApidata(data);
        const newData = JSON.parse(JSON.stringify(data));
        setSorted(newData.sort(namesorting));
    })

  }, []);
  const namesorting = (a, b) => {
    if (a.name < b.name) return -1;
    if (a.name > b.name) return 1;
    return 0;
  }

  const agesorting = (d1,d2)=> {
    var dt1= new Date(d1.dob.slice(6)+"/"+d1.dob.slice(3,5)+"/"+d1.dob.slice(0,2));
    var dt2= new Date(d2.dob.slice(6)+"/"+d2.dob.slice(3,5)+"/"+d2.dob.slice(0,2));
    if(dt1<=dt2){
      return 1;
    }else{
      return -1;
    }

  }
  

  return (
    <div className="App">
      <ShowData changeColor = {""} Apidata = {Apidata} setApidata = {setApidata} dataType = {dataType} setDataType = {setDataType}/>
      <p></p>
      <button onClick={()=>{
          setDataType("Shuffled data");
        const newData = JSON.parse(JSON.stringify(Apidata));
          setApidata(newData.sort( ()=>{return Math.random()-0.5 }));
      }}>
          Shuffle
      </button>

      <p></p>
      <button onClick={()=>{
        const newData = JSON.parse(JSON.stringify(Apidata));
        setSorted(newData.sort(namesorting));
        setSortedType("Sorted By Name");
        setsortedColorChange("THsortedByName")
      }}>
        Name Sorting 
      </button>
      <button onClick={()=>{
        const newData = JSON.parse(JSON.stringify(Apidata));
        setSorted(newData.sort(agesorting));
        setSortedType("Sorted By Age");
        setsortedColorChange("THsortedByAge")

      }}>
      Age Sorting 
      </button>
      <p></p>
      <ShowData changeColor = {sortedColorChange} Apidata = {sorted} setApidata = {setSorted} dataType = {sortedType} setDataType = {setSortedType}/>
      
    </div>
  );
}

export default App;