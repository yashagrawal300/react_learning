import React from 'react';

function ShowData(props) {

  return (<div>
   
   <h2>{props.dataType}</h2>
      <table border = "1px solid black">
    

          <tbody>
            
              <tr>
                  <th className={props.changeColor}>Name</th>
                  <th className={props.changeColor}>Date of Birthday</th>
              </tr>

              {
                  props.Apidata.map((item)=>{

                    return(
                        <tr key={item.name}>
                            <td>{item.name}</td>
                            <td>{item.dob}</td>

                        </tr>
                    )
                  })
              }


          </tbody>

      </table>



  </div>);
}

export default ShowData;
