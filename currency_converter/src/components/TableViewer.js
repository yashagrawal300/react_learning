import {useState, useEffect} from "react";


export default function TableViewer() {
    const url = "https://jsonplaceholder.typicode.com/posts";
    const [apiData, setApiData] = useState([]);
    var [count, setCount] = useState(0);
    useEffect(()=>{

      fetch(url, {
        method:"get"
      }).then(response => response.json())
      .then((data)=>{
        setApiData(data);

      }).catch(error=>{
        console.log(error);
      })

    }, [])

  return (
  <div className="centerit">


    <table border = "1px solid black">
      <tbody>
      <tr>
        <th>UserId</th>
        <th>id</th>
        <th>title</th>
        <th>body</th>
      </tr>

      {
        apiData.slice(count, count+10).map((item)=>{
          return(
            <tr key = {item.id}>
              <td>{item.userId}</td>
              <td>{item.id}</td>
              <td>{item.title.slice(0, 30)}</td>
              <td>{item.body.slice(0, 30)}</td>

            </tr>
          )
        })
      }

</tbody>

    </table>
    <p></p>

    <button onClick={()=>{
      if(count===0){
        setCount(apiData.length - 10)
      }
      else{
        setCount(count-10)
      }
    }}>Back</button>&nbsp;

    <button onClick={()=>{
      if(count === apiData.length-10){
        setCount(0)
      }
      else{
        setCount(count+10);
      }

    }}>Front</button>
    
      



  </div>);
}
