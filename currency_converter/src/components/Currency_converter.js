import React from 'react';
import { useState , useEffect} from 'react';

export default function Currency_converter() {
    const [inpCurr, setinpCurr] = useState(0);
    const [inpCurrType, setinpCurrType] = useState("INR");
    const [outCurr, setoutCurr] = useState(0);
    const [outCurrType, setoutCurrType] = useState("INR");
    const options = ["INR", "USD", "CD", "DINAR", "EURO"];
    var flags = {"INR":"🇮🇳","USD":"🇺🇸","EURO":"🇩🇪","DINAR":"🇪🇬","CD":"🇨🇦"};
    const optionsRenderInput =[];
    const optionsRenderOutput =[];

    useEffect(() => {
        convert();
    }, [inpCurr, inpCurrType, outCurr, outCurrType]);
    

    const convert = ()=>{
       const currencies = {
           "INR": 1,
           "EURO": 84.34,
           "USD": 74.77,
           "DINAR": 247.5,
           "CD": 59.11
       }
       const answer = Math.round((inpCurr*currencies[inpCurrType]/currencies[outCurrType] )* 100)/100;
       setoutCurr(answer);
        }
    
    for(let i =0 ; i <options.length; i++){
        optionsRenderInput.push(
            <option key={options[i]} value={options[i]}>{options[i]+ flags[options[i]]}</option>

        )
    }

    for(let i =0 ; i <options.length; i++){
        optionsRenderOutput.push(
            <option key={options[i]} value={options[i]}>{options[i] + flags[options[i]]}</option>

        )
    }


  return(
  <div className='currency'>
      <center> <h1>Currency Converter</h1></center>
            <input id = "inputCurr" type = "text" onChange={evnt=>{
          setinpCurr(evnt.target.value);
          convert()
      }}/>
      <select className = "flag"  value={inpCurrType} onChange={evnt=>{

setinpCurrType(evnt.target.value)
convert()

      }}>
          {optionsRenderInput}
      </select>
      <p></p>   
      <p></p>      

      <input placeholder={outCurr} type = "text" disabled/>
      <select value={outCurrType} className = "flag" onChange={evnt=>{

setoutCurrType(evnt.target.value)
convert()

      }}>
          {optionsRenderOutput}
      </select>




  </div>);
}
