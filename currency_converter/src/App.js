import './App.css';
import Currency_converter from './components/Currency_converter';
import TableViewer from './components/TableViewer';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
    <ul>
    <li><a href = "/cc">Currency Converter</a></li>
    <li ><a href = "/tv">API data table</a></li>
  
</ul>
<p></p>
    <Routes>
    <Route path="/" element={<Currency_converter></Currency_converter>}>
      </Route>
    <Route path="/cc" element={<Currency_converter />}>
      </Route>

      <Route path="/tv" element={<TableViewer />}>
      </Route>


    </Routes>
    
    </BrowserRouter>

  );
}

export default App;
