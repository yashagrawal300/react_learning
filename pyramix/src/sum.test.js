const sum = require("./components/Sum");

test("adds 1 + 2  to equals to 3", () =>{
    expect(sum(1, 2)).toBe(3);
})



it("12 + 12  = 24", ()=>{
    expect(sum(12, 12)).toBe(24);

})


it("Testing string in argumentes for exceptios handling", ()=>{
    expect(sum("1", 1)).toBe(2);
    
})